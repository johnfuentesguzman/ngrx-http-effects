import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  usersData$: Observable<any>;
  constructor(private userService: UserService){}

  ngOnInit(){
    this.userService.getUserData(1, 20).subscribe(users => {
      console.log('firebase map test.... ' , users)
    });
  }
}
