import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersCardComponent } from './components/users-card/users-card.component'
import { UserService } from './services/user.service';
import { userReducer } from './store/Reducers/user.reducer'


@NgModule({
  declarations: [
    AppComponent,
    UsersCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({users: userReducer}),
    StoreDevtoolsModule.instrument({})
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
