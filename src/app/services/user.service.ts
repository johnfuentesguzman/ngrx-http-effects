import { Injectable } from '@angular/core';
import { take, map, tap, catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { HttpClient } from '@angular/common/http';

import * as UserActions from  '../store/Actions/users.actions';
import { Users } from './../models/users.interface';

@Injectable()
export class UserService {
  private readonly url = 'https://randomuser.me/api/';

  constructor(
    private http: HttpClient,
    private store: Store,
  ) {}

  getUserData(pagination, numberOfResultPerRequest){
    return this.http.get<Users[]>(this.url)
    .pipe(
      map(res =>  {
        console.log('HTTP response successfully', res);
        this.store.dispatch(new UserActions.fetchUsers(res));
      }),
      catchError(err => {
              console.log('Handling error locally and rethrowing it...', err);
              return (err);
          })
      )
  
  }
}
