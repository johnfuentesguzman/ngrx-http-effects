import { Action } from '@ngrx/store';
import { Users } from '../../models/users.interface';


export const FETCH_USERS = '[Users] get Available Users';

export class fetchUsers implements Action {
  readonly type = FETCH_USERS;

  constructor(public payload: Users[]) {}
}


export type UsersActions =
  | fetchUsers
