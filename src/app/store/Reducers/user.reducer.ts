import { Action, createFeatureSelector, createSelector } from '@ngrx/store';

import {UsersActions, FETCH_USERS} from '../Actions/users.actions';
import { Users } from '../../models/users.interface';



export interface UsersState {
    fetchUsers: Users[];
    loading: boolean;
}
export interface State {
    users: UsersState;
}

const initialState: UsersState = {
  fetchUsers: [],
  loading: true
};

export function userReducer(state = initialState, action: UsersActions) { // musrt be declared in app.reducer.ts
  switch (action.type) {
    case FETCH_USERS:
      return {
        ...state,
        fetchUsers: action.payload,
        loading: false
      };
    default: {
      return state;
    }
  }
}

export const getUsersDara = createFeatureSelector<UsersState>('users'); //variable to indenty a unique id for hadling next selectos , in this case would be "users"

//selectors: 
export const getAvailableUsers = createSelector(getUsersDara, (state: UsersState) => state.fetchUsers);


