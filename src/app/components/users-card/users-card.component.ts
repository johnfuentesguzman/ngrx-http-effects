import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './users-card.component.html',
  styleUrls: ['./users-card.component.scss']
})
export class UsersCardComponent implements OnInit{
  usersData$: Observable<any>;
  constructor(private userService: UserService){}

  ngOnInit(){
    // this.userService.getUserData(1, 20).subscribe(users => {
    //   console.log('firebase map test.... ' , users)
    // });
  }
}
