import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersCardComponent } from './components/users-card/users-card.component';
//import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  { path: '', component: UsersCardComponent },
  //{ path: 'training', loadChildren: './training/training.module#TrainingModule', canLoad: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  //providers: [AuthGuard]
})
export class AppRoutingModule {}
